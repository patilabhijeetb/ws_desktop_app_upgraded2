/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workshiftly.common.constant;

/**
 *
 * @author chamara
 */
public enum StatusCode {
    SUCCESS,
    SESSION_INVALID,
    VALIDATION_ERROR,
    APPLICATION_ERROR,
    NETWORK_ERROR,
    DATABASE_EXCEPTION,
    DATABASE_ERROR,
    NOT_FOUND,
    BAD_REQUEST,
    SYNC_ERROR,
    TASK_ALREADY_STOPPED,
    TASK_ALREADY_COMPLETED,
    TASK_NOT_FOUND,
    PROJECT_NOT_FOUND,
    TASK_VIEWMODEL_NOT_FOUND,
    FILE_ALREADY_DOWNLOADING,
    NULL_REFERENCE_ERROR,
    PLATFORM_NOT_SUPORRTED;
}
